#pragma once
#include <string>
#include <vector>
#include "LookCommand.h"
#include "GoCommand.h"
#include "InventoryCommand.h"
#include "HelpCommand.h"
#include "AliasCommand.h"
#include "TakeCommand.h"
#include "PutCommand.h"
#include "OpenCommand.h"

using namespace std;

class CommandProcessor
{
private:
	string _query;
	vector<Command*> _commands;
	Command* selectCommand(string);
public:
	CommandProcessor();
	~CommandProcessor();

	void Execute(Player*, string);

};

