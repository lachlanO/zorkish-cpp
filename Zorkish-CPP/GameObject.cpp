#include "GameObject.h"
#include <iostream>

#include "Inventory.h"
#include "OpenSender.h"






GameObject::GameObject(vector<string> ids, string name, string desc)
{
	for (int i = 0; i < ids.size(); i++) {
		_ids.push_back(ids[i]);
	}
	_name = name;
	_desc = desc;
	string inventoryID = _name + "'s Inventory";
	_inventory = new Inventory(0, { inventoryID});
	_open = true;
	_subscribedToBlackboard = false;

	//ListenToSender(OpenSender::getInstance());


}

GameObject::GameObject(vector<string> ids, string name, string desc, int size, bool open)
{
	for (int i = 0; i < ids.size(); i++) {
		_ids.push_back(ids[i]);
	}
	_name = name;
	_desc = desc;
	string inventoryID = _name + "'s Inventory";
	_inventory = new Inventory(size, { inventoryID });
	_open = open;

	_subscribedToBlackboard = false;

	//ListenToSender(OpenSender::getInstance());
}


GameObject::~GameObject()
{
}


bool GameObject:: AreYou(string objectID) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (objectID == _ids[i]) areYou = true;
	}
	return areYou;
}

void GameObject::AddID(string nuID) {
	_ids.push_back(nuID);
}

string GameObject::GetDesc() {
	return _desc;
}


string GameObject::GetName() {
	return _name;
}


Inventory * GameObject::MyInventory() {
	return _inventory;
}

GameObject* GameObject::Locate(string id) {
	if (_inventory->HasItem(id)) {
		return _inventory->Fetch(id);
	}

	GameObject *temp = new GameObject({ "error" }, "error", "error");
	return temp;
}

bool GameObject::AreYouOpen() {
	return _open;
}

void GameObject::Open() {
	_open = true;
}

void GameObject::Close() {
	_open = false;
}

void GameObject::ChangeName(string nuName) {
	_name = nuName;
}

void GameObject::Update()
{
	//vector<string> commands = GetMessage();


	vector<string> commands = GetBlackboardMessage();
	int item = 10000;
	
	if (_subscribedToBlackboard) {

		for (int i = 0; i < commands.size(); i++) {
			if (AreYou(commands[i])) {
				item = i;
				if (_open) {
					cout << "[" << _name << "] is allready open" << endl << endl << endl;
				}
				else {
					Open();
					cout << "[" << _name << "] successfully opened" << endl << endl << endl;
				}
			}
		}
		
		if (item != 10000) {
			RemoveMessage(item);
		}
		
	}

	
}

void GameObject::SubscribeToBlackBoard()
{
	_subscribedToBlackboard = true;
}
