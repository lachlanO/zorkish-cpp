#include "TakeCommand.h"
#include <iostream>
#include <sstream>
#include <algorithm>
#include "SingletonHealth.h"

using namespace std;


TakeCommand::TakeCommand()
{
	_ids = { "take", "pickup" };
}


TakeCommand::~TakeCommand()
{
}

void TakeCommand::AddID(string nuid) {
	_ids.push_back(nuid);
}

string TakeCommand::Syntax() {
	string mySyntax = "[ ";

	for (auto it = _ids.begin(); it != _ids.end(); ++it)
	{
		if (it == _ids.end())
		{
			mySyntax += *it;
		}
		else {
			mySyntax += *it + ", ";
		}
	}
	mySyntax += "] _, [from] _,";

	return mySyntax;
}


void TakeCommand::AddCommand(Command*) {

}


bool TakeCommand::AreYou(string objectID) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (objectID == _ids[i]) areYou = true;
	}
	return areYou;
}


void TakeCommand::Execute(Player *p, string text) {
	vector<string> command;

	IHaveInventory *container;
	string itemID = "";
	string containerID;

	std::transform(text.begin(), text.end(), text.begin(), ::tolower);
	
	istringstream iss(text);
	string nu;
	while (getline(iss, nu, ' ')) {

		command.push_back(nu);
	}

	if (AreYou(command[0])) {
		switch (command.size()) {
		case 2:
			itemID = command[1];
			moveItem(p, itemID, p->MyLocation());
			break;

		case 4:
			itemID = command[1];
			containerID = command[3];
			if (fetchContainer(p, containerID) != nullptr) {
				moveItem(p, itemID, fetchContainer(p, containerID));
			}
			else {
				cout << "Invalid conatainer" << endl;
			}
			break;

		default:
			cout << "Incorrect number of words for take command, input should either be 2 or 4 words" << endl;
			break;
		}
	}
}


IHaveInventory* TakeCommand:: fetchContainer(Player *p, string containerID) {
	if (p->AreYou(containerID)) {
		return p;
	}
	else if (p->MyLocation()->AreYou(containerID)) {
		return p->MyLocation();
	}
	else if (p->MyInventory()->HasItem(containerID)) {
		return p->Locate(containerID);
	}
	else {
		return nullptr;
	}
}

void TakeCommand:: moveItem(Player *p, string itemID, IHaveInventory *container) {
	SingletonHealth *myHealth;
	myHealth = SingletonHealth::getInstance();

	if (container->MyInventory()->HasItem(itemID)) {

		if (myHealth->HasHeal(itemID)) {
			myHealth->Heal(itemID);
			p->MyInventory()->Put(container->MyInventory()->Take(itemID));
			p->MyInventory()->Fetch(itemID)->ChangeName("Empty HealthPack");
			cout << "You successfully take and use the health pack [" << p->MyInventory()->Fetch(itemID)->GetName() << "]" << endl;
		}
		else {
			p->MyInventory()->Put(container->MyInventory()->Take(itemID));
			//p->MyInventory()->Fetch(itemID)->ListenToSender(OpenSender::getInstance());
			p->MyInventory()->Fetch(itemID)->SubscribeToBlackBoard();
			cout << "You have successfully moved [" << itemID << "] to your inventory" << endl;
		}
		
	}
	else {
		cout << "Could not find [" << itemID << "] in [" << container->GetName() << "]" << endl;
	}
}