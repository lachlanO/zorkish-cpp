#include "OpenBlackboardListener.h"



OpenBlackboardListener::OpenBlackboardListener()
{
	_messageboard = Blackboard::getInstance();
}


OpenBlackboardListener::~OpenBlackboardListener()
{
}

vector<string> OpenBlackboardListener::GetBlackboardMessage()
{
	vector<OpenMessage> example = *_messageboard->GetOpenMessage();

	vector<string> returnValues;

	for (int i = 0; i < example.size(); i++) {
		returnValues.push_back(example[i].GetCommand());
	}

	return returnValues;
}

void OpenBlackboardListener::RemoveMessage(int id)
{
	_messageboard->GetOpenMessage()->erase(_messageboard->GetOpenMessage()->begin() + id);
}
