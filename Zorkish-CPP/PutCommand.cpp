#include "PutCommand.h"

#include<sstream>
#include<iostream>
#include<algorithm>
#include "Blackboard.h"



PutCommand::PutCommand()
{
	_ids = { "put" };
}


PutCommand::~PutCommand()
{
}

bool PutCommand::AreYou(string objectID) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (objectID == _ids[i]) areYou = true;
	}
	return areYou;
}

void PutCommand::Execute(Player *p, string text) {
	vector<string> command;

	IHaveInventory *container;
	string itemID = "";
	string containerID;

	std::transform(text.begin(), text.end(), text.begin(), ::tolower);

	istringstream iss(text);
	string nu;
	while (getline(iss, nu, ' ')) {

		command.push_back(nu);
	}

	if (AreYou(command[0])) {
		switch (command.size()) {
		case 2:
			itemID = command[1];
			container = p->MyLocation();
			moveItem(p, itemID, container);
			break;

		case 4:
			itemID = command[1];
			containerID = command[3];
			if (fetchContainer(p, containerID) != nullptr) {
				moveItem(p, itemID, fetchContainer(p, containerID));
			}
			else {
				cout << "Invalid conatainer" << endl;
			}
			break;

		default:
			cout << "Invalid put command [should either be 2 or 4 words in length]" << endl;
			break;
		}
	}
	else {
		cout << "Invalid Put command formatting" << endl;
	}
	

}

IHaveInventory* PutCommand::fetchContainer(Player *p, string containerID) {
	if (p->AreYou(containerID)) {
		return p;
	}
	else if (p->MyLocation()->AreYou(containerID)) {
		return p->MyLocation();
	}
	else if (p->MyInventory()->HasItem(containerID)) {
		return p->Locate(containerID);
	}
	else {
		return nullptr;
	}
}

void PutCommand::moveItem(Player *p, string itemID, IHaveInventory *container) {
	if (p->MyInventory()->HasItem(itemID)) {

		if (container->AreYouOpen()) {
			container->MyInventory()->Put(p->MyInventory()->Take(itemID));
			//p->MyInventory()->Fetch(itemID)->ListenToSender(OpenSender::getInstance());
			p->MyInventory()->Fetch(itemID)->SubscribeToBlackBoard();

			

			cout << "You have successfully moved [" << itemID << "] to [" << container->GetName() << "]" << endl;
		}
		else {
			cout << container->GetName() << " is closed";
		}
	}
	else {
		cout << "Could not find [" << itemID << "] in [" << container->GetName() << "]" << endl;
	}
}



void PutCommand::AddID(string nuid) {
	_ids.push_back(nuid);
}

string PutCommand::Syntax() {
	string mySyntax = "[ ";

	for (auto it = _ids.begin(); it != _ids.end(); ++it)
	{
		if (it == _ids.end())
		{
			mySyntax += *it;
		}
		else {
			mySyntax += *it + ", ";
		}
	}
	mySyntax += "] _, [in] _,";

	return mySyntax;
}


void PutCommand::AddCommand(Command*) {

}