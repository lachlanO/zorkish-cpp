#pragma once
#include "GameObject.h"
#include <vector>

class Inventory
{
private:
	const int _CAPACITY;
	vector<GameObject*> _myObjects;
	vector<string> _ids;

public:
	Inventory(int, vector<string>);
	~Inventory();
	void ItemList();
	bool HasItem(string id);
	void Put(GameObject*);
	GameObject* Take(string id);
	GameObject* Fetch(string id);
	vector<GameObject*> Contents();

	void Update();
	void AddToOpenListen();

};

