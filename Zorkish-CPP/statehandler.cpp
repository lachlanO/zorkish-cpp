#include "statehandler.h"
#include "menu.h"




statehandler::statehandler(bool *gameRunning)
{
	myState = new menu(this);
	running = gameRunning;
}


statehandler::~statehandler()
{
}

void statehandler::Update() {
	myState->Update();
}

void statehandler::Render() {

}


void statehandler::ChangeGameState() {
	*running = false;
}

void statehandler::ChangeState(state *newState) {
	delete(myState);
	myState = newState;
}