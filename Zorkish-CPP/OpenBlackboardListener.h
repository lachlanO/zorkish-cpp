#pragma once
#include "Blackboard.h"

class OpenBlackboardListener
{
private:
	Blackboard * _messageboard;
public:
	OpenBlackboardListener();
	~OpenBlackboardListener();
	vector<string> GetBlackboardMessage();
	void RemoveMessage(int id);
};

