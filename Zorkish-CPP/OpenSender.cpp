#include "OpenSender.h"




bool OpenSender::instanceFlag = false;

OpenSender* OpenSender::single = NULL;
OpenSender* OpenSender::getInstance()
{
	if (!instanceFlag)
	{
		single = new OpenSender();
		instanceFlag = true;
		return single;
	}
	else
	{
		return single;
	}
}

void OpenSender::sendMessage(string command)
{
	OpenMessage newMessage = OpenMessage(command);
	
	for (int i = 0; i < _listeners.size(); i++) {
		_listeners[i]->RecieveMessage(newMessage);
	}
}

void OpenSender::RecieveListener(OpenListener *  listener)
{
	_listeners.push_back(listener);
}
