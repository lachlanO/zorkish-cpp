#pragma once
#include "GameObject.h"
#include "IHeal.h"
#include "Player.h"

class HealthPack: public IHeal, public GameObject
{
private:
	int _healValue;
	Player *_p;
public:
	HealthPack(vector<string>, string, string, Player*,int);
	~HealthPack();

	bool AreYou(string);
	void Heal(string);
};

