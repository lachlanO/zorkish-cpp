#include "Inventory.h"
#include <iostream>
#include <string>

using namespace std;

Inventory::Inventory(int max, vector<string> ids) : _CAPACITY(max)
{
	for (int i = 0; i < ids.size(); i++) {
		_ids.push_back(ids[i]);
	}
}


Inventory::~Inventory()
{
}

void Inventory::ItemList() {
	string temp = _ids[0];
	cout << _ids[0] << " contains:" << endl;
	for (int i = 0; i < _myObjects.size(); i++) {
		cout << "Item " << i + 1 << ": ";
		cout << _myObjects[i]->GetName() << endl;
	}
	cout << endl;
}

void Inventory::Put(GameObject *nuItem) {
	_myObjects.push_back(nuItem);
}

GameObject* Inventory::Take(string id) {
	int temp = 0;
	int check = false;

	for (int i = 0; i < _myObjects.size(); i++) {
		if (_myObjects[i]->AreYou(id)) {
			temp = i;
			check = true;
		}
	}

	if (check) {
		GameObject *takeItem = _myObjects[temp];
		_myObjects.erase(_myObjects.begin() + temp);

		return takeItem;
	}
	GameObject *error = new GameObject({ "error" }, "error", "error");
	return error;
}


GameObject* Inventory::Fetch(string id) {
	int temp;
	for (int i = 0; i < _myObjects.size(); i++) 
	{
		if (_myObjects[i]->AreYou(id)) {
			temp = i;
		}
	}
	return _myObjects[temp];
}

vector<GameObject*> Inventory::Contents()
{
	return _myObjects;
}

void Inventory::Update()
{
	for (int i = 0; i < _myObjects.size(); i++) {
		_myObjects[i]->Update();
	}
}

void Inventory::AddToOpenListen()
{
	for (auto g : _myObjects) {
		g->ListenToSender(OpenSender::getInstance());
	}
}

bool Inventory::HasItem(string id) {
	for (int i = 0; i < _myObjects.size(); i++)
	{
		if (_myObjects[i]->AreYou(id)) {
			return true;
		}
	}
	return false;
}