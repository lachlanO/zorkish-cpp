#pragma once

#include "about.h"
#include "adventure.h"
#include "bestscore.h"
#include "gameplay.h"
#include "help.h"
#include "highscore.h"
#include <iostream>
#include <ctime>
#include "time.h"
#include <sstream>
#include <stdlib.h>
#include <string>
#include <array>
#include <conio.h>
#include <cmath>
#include <vector>


class menu: public state
{
private:
	void validate();
	string command;
	bool commandValid;

public:
	menu(statehandler *controler);
	~menu();
	void Update();
	void Render();
	void print();
};

