#pragma once
#include "Inventory.h"
#include "Location.h"


class Player: public IHaveInventory
{
private:
	Inventory *_inventory;
	string _name;
	string _desc;
	Location *_location;
	bool _open;
	int _health;

public:
	Player(string name, string desc, int);
	~Player();
	Inventory * MyInventory();
	GameObject* Locate(string);
	void AddToInv(GameObject*);
	Location* MyLocation();
	void ChangeLocation(string id);
	void ChangeLocation(Location*);
	string GetName();
	bool AreYou(string);

	bool AreYouOpen();

	void ChangeHealth(int);

	void Open();
	void Close();
	void Update();
	void AddToOpenListen();

};

