#pragma once
#include "OpenListener.h"

class OpenSender
{
private:
	vector<OpenListener*> _listeners;
	static bool instanceFlag;
	static OpenSender *single;
	OpenSender()
	{

	}
public:
	static OpenSender* getInstance();
	void sendMessage(string command);
	void RecieveListener(OpenListener*);
	~OpenSender()
	{
		instanceFlag = false;
	}
};

