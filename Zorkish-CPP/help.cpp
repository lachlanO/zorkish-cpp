#include "menu.h"
#include <iostream>
#include <string>
//#include "stdafx.h"


help::help(statehandler *controler) : state(controler)
{
	
	this->validCommands[0] = "quit";
	this->validCommands[1] = "hiscore";
}

help::~help()
{

}

void help::Update() 
{
	cout << "Zorkish :: Help" << endl;
	cout << "--------------------" << endl << endl;

	cout << "The folowing commands are supported:" << endl << endl;
	
	//This should be implemented using the array in parent however not really needed atm
	cout << "   quit or q" << endl;
	cout << "   hiscore" << endl;
	cout << " [go] _, (or just n, ne, e, etc)" << endl << endl;

	//implement the rest of the commands later

	cout << "Press Enter to return to the Main Menu";

	_getch();
	_getch();

	cout << endl << endl << endl << endl;

	ourState->ChangeState(new menu(ourState));
}

void help::Render() 
{

}

void help::validate() {

}
