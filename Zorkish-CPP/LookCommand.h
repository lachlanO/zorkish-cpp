#pragma once
#include "Command.h"
#include "IHaveInventory.h"


class LookCommand: public Command
{
private:
	vector<string> _ids;
	IHaveInventory* fetchContainer(Player*, string);
public:
	LookCommand();
	~LookCommand();

	void Execute(Player*, string);
	bool AreYou(string);
	string Syntax();
	void AddID(string);
	void AddCommand(Command*);
};

