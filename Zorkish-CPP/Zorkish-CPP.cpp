// Zorkish.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <ctime>
#include "time.h"
#include <sstream>
#include <stdlib.h>
#include <string>
#include <array>
#include "statehandler.h"

using namespace std;



int main()
{
	bool gameRunning = true;
	statehandler myGame = statehandler(&gameRunning);

	while (gameRunning) {


		myGame.Update();
	}


	return 0;
}

