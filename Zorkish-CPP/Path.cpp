#include "Path.h"



Path::Path(vector<string> ids, Location *location)
{
	_location = location;
	_ids = ids;
}


Path::~Path()
{
}



Location* Path::MyLocation() {
	return _location;
}

bool Path::AreYou(string id) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (id == _ids[i]) areYou = true;
	}
	return areYou;
}

void Path::AddID(string id) {
	_ids.push_back(id);
}
