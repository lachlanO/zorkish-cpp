#pragma once
#include "Command.h"


class TakeCommand: public Command
{
private:
	vector<string> _ids;
	IHaveInventory* fetchContainer(Player*, string);
	void moveItem(Player*, string, IHaveInventory*);

public:
	TakeCommand();
	~TakeCommand();

	void Execute(Player*, string);
	bool AreYou(string);
	string Syntax();
	void AddID(string);
	void AddCommand(Command*);
};

