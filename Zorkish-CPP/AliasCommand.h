#pragma once
#include "Command.h"

class AliasCommand: public Command
{
private:
	vector<string> _ids;
	vector<Command*> _comms;
public:
	AliasCommand(vector<Command*>);
	~AliasCommand();

	void Execute(Player*, string);
	bool AreYou(string);
	string Syntax();
	void AddID(string);
	void AddCommand(Command*);
};

