#pragma once
#include "Inventory.h"
#include <vector>
#include "Path.h"
#include "IHaveInventory.h"

class Location: public IHaveInventory
{
private:
	Inventory * _inventory;
	vector<string> _ids;
	string _name;
	string _desc;
	vector<Path> _paths;
	int number;
	bool _open;

public:
	Location(vector<string>, string, string);
	Location();
	GameObject* Locate(string);
	Inventory * MyInventory();
	string GetName();
	~Location();
	bool AreYou(string id);
	void AddIdentifier(string id);
	void AddPath(Path);
	vector<Path> GetPaths();

	bool AreYouOpen();

	void Open();
	void Close();
	void Update();
};

