#pragma once

#include "state.h"
#include <iostream>
#include <ctime>
#include "time.h"
#include <sstream>
#include <stdlib.h>
#include <string>
#include <array>

class about: public state
{
private:
	void validate();
public:
	about(statehandler *controler);
	~about();
	void Update();
	void Render();
};

