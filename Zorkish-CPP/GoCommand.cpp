#include "GoCommand.h"
#include <algorithm>
#include <sstream>
#include <iostream>


GoCommand::GoCommand()
{
	_ids = { "move", "go", "head", "leave" };
}


GoCommand::~GoCommand()
{
}

bool GoCommand::AreYou(string objectID) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (objectID == _ids[i]) areYou = true;
	}
	return areYou;
}


Path GoCommand::getPath(Player *p, string direction) {

	vector<Path> temp = p->MyLocation()->GetPaths();

	for (int i = 0; i < temp.size(); i++) {
		if (temp[i].AreYou(direction)) {
			return temp[i];
		}
	}
	return Path({ "error" }, nullptr);
}

void GoCommand::Execute(Player *p, string text) {

	vector<string> command;

	std::transform(text.begin(), text.end(), text.begin(), ::tolower);

	istringstream iss(text);
	string nu;
	while (getline(iss, nu, ' ')) {

		command.push_back(nu);
	}

	if (command.size() == 2) {
		if (AreYou(command[0])) {
			Path path = getPath(p, command[1]);

			if (path.AreYou("error")) {
				cout << "Cannot find location [" << command[1] << "]" << endl;
			}

			p->ChangeLocation(command[1]);

			cout << "You successfull navigate to " << p->MyLocation()->GetName() << endl;


		}
		else {
			cout << "Incorrect input for move command" << endl;
		}
		}
	else {
		cout << "Incorrect input for move command [note: command should only be 2 words]" << endl;
	}
}


void GoCommand::AddID(string nuid) {
	_ids.push_back(nuid);
}

string GoCommand::Syntax() {
	string mySyntax = "[ ";

	for (auto it = _ids.begin(); it != _ids.end(); ++it)
	{
		if (it ==_ids.end())
		{
			mySyntax += *it;
		}
		else {
			mySyntax += *it + ", ";
		}
	}
	mySyntax += "] _,";

	return mySyntax;
}




void GoCommand::AddCommand(Command*) {

}
