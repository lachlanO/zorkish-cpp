#pragma once
#include <string>
using namespace std;
class IHeal
{
public:
	virtual void Heal(string id) = 0;
	virtual bool AreYou(string) = 0;
};

