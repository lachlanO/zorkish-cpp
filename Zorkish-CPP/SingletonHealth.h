#pragma once
#include "IHeal.h"
#include <vector>
#include <string>

using namespace std;

class SingletonHealth: public IHeal
{
private:
	vector<IHeal*> _heals;
	static vector<string> _ids;
	static bool instanceFlag;
	static SingletonHealth *single;
	SingletonHealth()
	{
		
	}
	void remove(string);
public:
	static SingletonHealth* getInstance();
	vector<IHeal*> myList();
	~SingletonHealth()
	{
		instanceFlag = false;
	}

	void add(IHeal*);

	bool HasHeal(string);

	void Heal(string);

	bool AreYou(string);
};

