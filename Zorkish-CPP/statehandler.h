#pragma once
//#include "menu.h"zz

class state;

class statehandler
{
private:
	state *myState;
	bool *running;

public:
	statehandler(bool *gameRunning);
	~statehandler();
	void Update();
	void Render();
	void ChangeGameState();
	void ChangeState(state *newState);
};

