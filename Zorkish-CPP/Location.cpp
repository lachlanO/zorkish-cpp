#include "Location.h"

static int num = 0;

Location::Location(vector<string> ids, string name, string desc)
{
	_name = name;
	_desc = desc;
	_ids = ids;
	_open = true;

	_inventory = new Inventory(100, { "locInv" });

	num++;
	number = num;
	
}

Location::Location() {

}


Location::~Location()
{
}

bool Location::AreYou(string id) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (id == _ids[i]) areYou = true;
	}
	return areYou;
}

GameObject* Location::Locate(string id) {
	if (_inventory->HasItem(id)) {
		return _inventory->Fetch(id);
	}
	return nullptr;
}

Inventory* Location::MyInventory() {
	return _inventory;
}

void Location::AddIdentifier(string id) {
	_ids.push_back(id);
}


string Location::GetName() {
	return _name;
}


void Location::AddPath(Path path) {
	_paths.push_back(path);
}

vector<Path> Location::GetPaths() {
	return _paths;
}

bool Location::AreYouOpen() {
	return _open;
}

void Location::Open() {

}

void Location::Close() {

}

void Location::Update()
{
	_inventory->Update();
}
