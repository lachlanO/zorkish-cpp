#pragma once
#include <string>
#include <iostream>

using namespace std;

class GameObject;
class Inventory;

class IHaveInventory
{
public:

	virtual GameObject* Locate(string) = 0;

	virtual string GetName() = 0;

	virtual Inventory* MyInventory() = 0;

	virtual bool AreYouOpen() = 0;

	virtual void Open() = 0;
	virtual void Close() = 0;
};

