#pragma once
#include "Command.h"

class PutCommand: public Command
{
private:
	vector<string> _ids;
	void moveItem(Player*, string, IHaveInventory*);
	IHaveInventory* fetchContainer(Player*, string);
public:
	PutCommand();
	~PutCommand();

	void Execute(Player*, string);
	bool AreYou(string);
	string Syntax();
	void AddID(string);
	void AddCommand(Command*);
	

};

