#pragma once
#include <string>
#include <vector>

using namespace std;

class OpenMessage
{
private:
	string _command;
	
public:
	OpenMessage(string);
	~OpenMessage();
	string GetCommand();
};

