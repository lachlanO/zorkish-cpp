#include "CommandProcessor.h"
#include <sstream>
#include <iostream>
#include <algorithm>



CommandProcessor::CommandProcessor()
{
	_commands.push_back(new GoCommand());
	_commands.push_back(new TakeCommand());
	_commands.push_back(new PutCommand());
	_commands.push_back(new OpenCommand());
	_commands.push_back(new LookCommand());
	_commands.push_back(new InventoryCommand());
	_commands.push_back(new HelpCommand(_commands));
	_commands.push_back(new AliasCommand(_commands));

	_commands[3]->AddCommand(_commands[4]);
}


CommandProcessor::~CommandProcessor()
{
}

void CommandProcessor::Execute(Player *p, string text) {
	vector<string> command;


	string itemID = "";

	std::transform(text.begin(), text.end(), text.begin(), ::tolower);

	istringstream iss(text);
	string nu;
	while (getline(iss, nu, ' ')) {

		command.push_back(nu);
	}
	Command *comExe = selectCommand(command[0]);

	if (comExe != nullptr) {
		comExe->Execute(p, text);
	}
	else {
		cout << "Could not find appropriate function" << endl;
	}

	cout << endl;
}

Command* CommandProcessor::selectCommand(string id) {

	for (auto com : _commands) {
		if (com->AreYou(id)) {
			return com;
		}
	}


	return nullptr;
}
