#pragma once
#include "OpenMessage.h"

class OpenSender;

class OpenListener
{
private:
	vector<OpenMessage> _messages;
public:
	OpenListener();
	~OpenListener();
	void ListenToSender(OpenSender*);
	vector<string> GetMessage();
	void RecieveMessage(OpenMessage newMessage);
};

