#include <iostream>
#include <ctime>
#include "time.h"
#include <sstream>
#include <stdlib.h>
#include <string>
#include <array>
#include <fstream>
#include <sstream>
#include "Player.h"
#include "menu.h"

using namespace std;

adventure::adventure(statehandler *controler) : state(controler) {

}

adventure::~adventure() {

}

void adventure::Update() {
	cout << "Zorkish :: Adventure" << endl;
	cout << "--------------------" << endl << endl;

	cout << "Choose your adventure:" << endl << endl;

	cout << "   1. Mountain World" << endl;
	cout << "   2. Water World" << endl;
	cout << "   3. Box World" << endl << endl;

	cout << "Select 1-3:> ";
	cin >> command;

	//im too lazy to implement validation, sue me
	switch (command) {
	case 1:
		readFileAdventure1("Location.txt", "Path.txt", "Items.txt");
		break;

	case 2:
		readFileAdventure1("Location2.txt", "Path2.txt", "Items2.txt");
		break;

	case 3:
		readFileAdventure1("Location.txt", "Path.txt", "Items.txt");
		break;
	}
	
	

	//implement selection later
	ourState->ChangeState(new gameplay(ourState, _locations)); //get rid of this later just closes program
	cin.ignore();
	
}

void adventure::Render() {

}

void adventure::validate() {

}

void adventure::readFileAdventure1(string locationfile, string pathfile, string textfile) {

	//array<Location, 5> _locations;


	ifstream myLocations;
	myLocations.open(locationfile);
	vector<string> locationInfo;
	vector<string> ids;
	string currentLine;
	//int i = 0;
	if (myLocations) {
		while (getline(myLocations, currentLine)) {
			istringstream iss(currentLine);
			while (getline(iss, currentLine, '|')) {

				locationInfo.push_back(currentLine);
				
				if (locationInfo.size() == 3) {

					istringstream nu(locationInfo[2]);

					while (getline(nu, locationInfo[2], ',')) {

						ids.push_back(locationInfo[2]);
					}

					_locations.push_back(Location(ids, locationInfo[0], locationInfo[1]));

					ids.clear();
					locationInfo.clear();
					//i++;
					
				}
			}
			
		}
	}
	myLocations.close();

	//read in paths

	ifstream myPaths;
	myPaths.open(pathfile);
	vector<string> pathInfo;

	Location temp1 = Location({ "error" }, "error", "error");
	Location *temp2 = &temp1;
	int temp;
	Path nuPath = Path(ids, temp2);
	
	if (myPaths) {
		while (getline(myPaths, currentLine)) {

			istringstream nuiss(currentLine);
			while (getline(nuiss, currentLine, '|')) {
				pathInfo.push_back(currentLine);


				if (pathInfo.size() == 3) {

					istringstream nu(pathInfo[2]);

					while (getline(nu, pathInfo[2], ',')) {

						ids.push_back(pathInfo[2]);
					}

					temp1 = Location({ "error" }, "error", "error");
					temp2 = &temp1;
					temp = 0;
					

					for (int i = 0; i < _locations.size(); i++) {
						if (_locations[i].AreYou(pathInfo[0])) {
							temp1 = _locations[i];
							temp = i;
						}

						if (_locations[i].AreYou(pathInfo[1])) {
							temp2 = &_locations[i];
						}
					}
				
					nuPath =  Path(ids, temp2);
					temp1.AddPath(nuPath);

					_locations[temp] = temp1;
					ids.clear();
					pathInfo.clear();
				}
			}
		}
	}
	myPaths.close();

	ifstream myObjs;
	myObjs.open(textfile);
	vector<string> objInfo;
	int invSize = 0;
	bool amOpen = false;


	if (myObjs) {
		while (getline(myObjs, currentLine)) {

			istringstream nuiss(currentLine);
			while (getline(nuiss, currentLine, '|')) {
				objInfo.push_back(currentLine);

				if (objInfo.size() == 4) {
					invSize = stoi(objInfo[3]);
				}

				if (objInfo.size()==5) {
					if (objInfo[4] == "t") {
						amOpen = true;
					}
				}

				if (objInfo.size() == 6) {
					istringstream nu(objInfo[5]);

					while (getline(nu, objInfo[5], ',')) {

						ids.push_back(objInfo[5]);
					}

					for (auto s : _locations) {
						if (s.AreYou(objInfo[0])) {
							s.MyInventory()->Put(new GameObject(ids, objInfo[1], objInfo[2], invSize, amOpen));
						}
					}
					/*
					for (int i = 0; i < _locations.size(); i++) {
					if (_locations[i].AreYou(objInfo[0])) {
					_locations[i].MyInventory()->Put(new GameObject(ids, objInfo[1], objInfo[2], invSize, amOpen));
					}
					}
					*/
					invSize = 0;
					amOpen = false;
					ids.clear();
					objInfo.clear();
				}
				
				
			}
		}

	}
	myObjs.close();

}