#include "OpenListener.h"
#include "OpenSender.h"


OpenListener::OpenListener()
{
}


OpenListener::~OpenListener()
{
}

void OpenListener::ListenToSender(OpenSender * sender)
{
	sender->RecieveListener(this);
}


vector<string> OpenListener::GetMessage()
{
	vector<string> messageVector;

	for (int i = 0; i < _messages.size(); i++) {
		messageVector.push_back(_messages[i].GetCommand());
	}

	_messages.clear();

	return messageVector;
}

void OpenListener::RecieveMessage(OpenMessage newMessage)
{
	_messages.push_back(newMessage);
}

