#pragma once
#include "Command.h"


class InventoryCommand: public Command
{
private:
	vector<string> _ids;
public:
	InventoryCommand();
	~InventoryCommand();

	void Execute(Player*, string);
	bool AreYou(string);
	string Syntax();
	void AddID(string);
	void AddCommand(Command*);
};

