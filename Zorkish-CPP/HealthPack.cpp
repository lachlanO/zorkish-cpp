#include "HealthPack.h"
#include "SingletonHealth.h"



HealthPack::HealthPack(vector<string> ids, string name, string desc, Player*p, int healValue) : GameObject(ids, name, desc)
{
	_healValue = healValue;
	_p = p;

	SingletonHealth *s;
	s = SingletonHealth::getInstance();
	s->add(this);
}


HealthPack::~HealthPack()
{
}

void HealthPack::Heal(string id) {
	if (AreYou(id)) {
		_p->ChangeHealth(_healValue);
	}
	
}

bool HealthPack::AreYou(string id) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (id == _ids[i]) areYou = true;
	}
	return areYou;
}