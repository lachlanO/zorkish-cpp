#pragma once
#include "state.h"
const int ARRAY_SIZE = 10;

struct topScores {
	string name;
	int score;
	string map;
};

class bestscore: public state
{
private:
	void validate();
	topScores topTen[ARRAY_SIZE];

public:
	bestscore(statehandler *controler);
	~bestscore();
	void Update();
	void Render();

};

