#pragma once
#include "Command.h"

class CommandProcessor;

class HelpCommand: public Command
{
private:
	vector<Command*> _myCommands;
	vector<string> _ids;
public:
	HelpCommand(vector<Command*>);
	~HelpCommand();

	void Execute(Player*, string);
	bool AreYou(string);
	string Syntax();
	void AddID(string);
	void AddCommand(Command*);
};

