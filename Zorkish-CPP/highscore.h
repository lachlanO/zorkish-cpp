#pragma once

#include "state.h"

class highscore: public state
{
private:
	void validate();
	string name;

public:
	highscore(statehandler *controler);
	~highscore();
	void Update();
	void Render();
};

