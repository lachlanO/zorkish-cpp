#pragma once
#include "Player.h"
#include "state.h"
#include "Location.h"
#include "CommandProcessor.h"
#include "SingletonHealth.h"

class gameplay: public state
{
private:
	void validate();
	Player *_myPlayer;
	vector<Location> _locations;
	CommandProcessor _command;
	SingletonHealth *_myHeals;
public:
	gameplay(statehandler *controle, vector<Location>);
	~gameplay();
	void Update();
	void Render();
};

