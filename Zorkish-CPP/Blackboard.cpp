#include "Blackboard.h"

bool Blackboard::instanceFlag = false;

Blackboard* Blackboard::single = NULL;


Blackboard * Blackboard::getInstance()
{
	if (!instanceFlag)
	{
		single = new Blackboard();
		instanceFlag = true;
		return single;
	}
	else
	{
		return single;
	}
}

vector<OpenMessage>* Blackboard::GetOpenMessage()
{
	return &_OpenMessages;
}

void Blackboard::SendOpenMessage(string command)
{
	_OpenMessages.push_back(OpenMessage(command));
}
