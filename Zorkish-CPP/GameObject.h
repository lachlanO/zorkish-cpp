#pragma once
#include "stdafx.h"
#include <vector>
#include "IHaveInventory.h"
#include "OpenSender.h"
#include "OpenBlackboardListener.h"

class Inventory;

using namespace std;
class GameObject : public IHaveInventory, public OpenListener, public OpenBlackboardListener
{
protected:
	string _name;
	string _desc;
	vector<string> _ids;
	Inventory* _inventory;
	bool _open;
	bool _subscribedToBlackboard;

public:
	GameObject(vector<string>, string name, string desc);
	GameObject(vector<string>, string name, string desc, int size, bool open);
	~GameObject();

	string GetName();
	
	string GetDesc();

	bool AreYou(string objectID);

	void AddID(string nuID);

	GameObject* Locate(string);

	Inventory* MyInventory();

	bool AreYouOpen();

	void Open();
	void Close();

	void ChangeName(string);

	void Update();

	void SubscribeToBlackBoard();

};

