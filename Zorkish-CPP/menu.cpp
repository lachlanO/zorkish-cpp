#include "menu.h"
#include <stdlib.h>
#include <string>
#include "stdafx.h"


using namespace std;

menu::menu(statehandler *controler) : state(controler) {
	this->validCommands[0] = "quit";
	this->validCommands[1] = "1";
	this->validCommands[2] = "2";
	this->validCommands[3] = "3";
	this->validCommands[4] = "4";
	this->validCommands[5] = "q";

	commandValid = false;
}

menu::~menu()
{

}

void menu::Update() 
{
	cout << "Zorkish :: Main Menu" << endl;
	cout << "--------------------" << endl << endl;
	cout << "Welcome to Zorkish Adventures" << endl << endl;

	cout << "   1. Select Adventure and Play" << endl;
	cout << "   2. Hall of Fame" << endl;
	cout << "   3. Help" << endl;
	cout << "   4. About" << endl;
	cout << "   q. Quit" << endl << endl;

	cout << "Select command :> ";

	cin >> command;
	std::transform(command.begin(), command.end(), command.begin(), ::tolower);


	validate();

	cout << endl << endl;

	if (command == "1") {

		ourState->ChangeState(new adventure(ourState));
	}
	else if (command == "2") {
		ourState->ChangeState(new bestscore(ourState));
	}
	else if (command == "3") {
		ourState->ChangeState(new help(ourState));
	}
	else if (command == "4") {
		ourState->ChangeState(new about(ourState));
	}
	else if (command == "q" || command == "quit") {
		ourState->ChangeGameState();
	}
}

void menu::Render()
{
	print();
}

void menu::print() {
	cout << this->validCommands[0];
}

void menu::validate() {
	while (!commandValid) {
		for (int i = 0; i < 20; i++) {
			if (validCommands[i] == command) {
				commandValid = true;
			}
		}
		
		if (!commandValid) {
			cout << endl << endl << "Invalid command try again :> ";
			cin >> command;
			std::transform(command.begin(), command.end(), command.begin(), ::tolower);
		}

	}


}
