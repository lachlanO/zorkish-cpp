#include <iostream>
#include <ctime>
#include "time.h"
#include <sstream>
#include <stdlib.h>
#include <string>
#include <array>
#include "menu.h"

using namespace std;

about::about(statehandler *controler) : state(controler) {

}

about::~about() {

}

void about::Update() {
	cout << "Zorkish :: About" << endl;
	cout << "--------------------" << endl << endl;
	cout << "Written by: Lachlan O'Neill" << endl << endl;
	cout << "Press Enter to return to the Main Menu";

	_getch();
	_getch();

	cout << endl << endl << endl << endl;

	ourState->ChangeState(new menu(ourState));
}

void about::Render() {

}

void about::validate() {

}
