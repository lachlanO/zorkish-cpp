#pragma once
#include "stdafx.h"
#include <stdlib.h>
#include <string>
#include "statehandler.h"

using namespace std;

class state
{
private: 
	virtual void validate();

protected:
	string validCommands[20];
	statehandler *ourState;
public:
	state(statehandler *controler);
	~state();
	virtual void Update();
	virtual void Render();
};

