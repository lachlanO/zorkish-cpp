#pragma once
#include "Command.h"
#include "OpenSender.h"
#include "Blackboard.h"

class OpenCommand : public Command
{
private: 
	vector<string> _ids;
	IHaveInventory* fetchContainer(Player*, string);

	OpenSender* _sender;
	Blackboard* _blackboard;

public:
	OpenCommand();
	~OpenCommand();

	void Execute(Player*, string);
	bool AreYou(string);
	string Syntax();
	void AddID(string);
	void AddCommand(Command*);
};

