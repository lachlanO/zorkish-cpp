#pragma once
#include "stdafx.h"
#include "stdlib.h"
#include <string>
#include <vector>

using namespace std;

class Location;

class Path
{
private:
	vector<string> _ids;
	Location *_location;
public:
	Path(vector<string>, Location*);
	string GetLocationName();
	Location* MyLocation();
	bool AreYou(string);
	void AddID(string);
	~Path();
};

