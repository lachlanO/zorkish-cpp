#include "gameplay.h"
#include "HealthPack.h"

#include <iostream>

using namespace std;

gameplay::gameplay(statehandler *controler, vector<Location> locations) : state(controler)
{
	_myPlayer = new Player("me", "me", 4);
	_locations = locations;
	_myPlayer->ChangeLocation(&_locations[0]);

	_command = CommandProcessor();

	_myHeals = SingletonHealth::getInstance();
	

	_locations[0].MyInventory()->Put(new HealthPack({ "heal" }, "heal", "heal", _myPlayer, 5));
}


gameplay::~gameplay()
{
}

void gameplay::Update() {


	_myPlayer->AddToInv(new GameObject({ "1" }, "1", "1"));
	//_myPlayer->AddToInv(new GameObject({ "2" }, "2", "2"));
	//_myPlayer->AddToInv(new GameObject({ "3" }, "3", "3"));

	GameObject *example = _myPlayer->MyInventory()->Take("2");

	cout << "my current location - " << _myPlayer->MyLocation()->GetName() << endl;

	//_myPlayer->ChangeLocation("n");

	string temp;
	
	getline(cin, temp);


	_command.Execute(_myPlayer, temp);
	temp = "";

	for (auto l : _locations) {
		l.Update();
	}
	_myPlayer->Update();
}

void gameplay::Render() {

}

void gameplay::validate() {

}