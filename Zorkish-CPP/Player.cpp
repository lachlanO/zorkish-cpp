#include "Player.h"
#include <iostream>

using namespace std;

Player::Player(string name, string desc, int invSize)
{
	_inventory = new Inventory(invSize, { "myInventory", "ggez" });
	_name = name;
	_desc = desc;
	_open = true;
}


Player::~Player()
{
}

Inventory * Player::MyInventory() {
	return _inventory;
}

GameObject* Player::Locate(string id) {
	if (_inventory->HasItem(id)) {
		return _inventory->Fetch(id);
	}

	GameObject *temp = new GameObject({"error"}, "error", "error");
	return temp;
}

bool Player::AreYou(string name) {
	bool areYou = false;
	if (_name == name) {
		areYou = true;
	}
	return areYou;
}

void Player::AddToInv(GameObject *nuItem) {
	_inventory->Put(nuItem);
}

Location* Player::MyLocation() {
	return _location;
}

void Player::ChangeLocation(string id) {
	vector<Path> temp = _location->GetPaths();
	Location *nuLocation = nullptr;

	bool isChanged = false;

	for (int i = 0; i < temp.size(); i++) {
		if (temp[i].AreYou(id)) {
			nuLocation = temp[i].MyLocation();
		}
	}
	
	if (nuLocation != nullptr) {
		//delete(_location);			ask why this doesnt work
		_location = nuLocation;
		isChanged = true;
	}
	
	if (!isChanged) {
		cout << "Invalid location, no location with id = " << id << " is connected to your current location" << endl;
	}
}




void Player::ChangeLocation(Location* nuLoc) {
	_location = nuLoc;
}

string Player :: GetName() {
	return _name;
}




bool Player::AreYouOpen() {
	return _open;
}

void Player::Open() {

}

void Player::Close() {

}

void Player::Update()
{
	//AddToOpenListen();
	_inventory->Update();
}

void Player::AddToOpenListen()
{
	_inventory->AddToOpenListen();
}


void Player::ChangeHealth(int change) {

	_health += change;
}

