#include "AliasCommand.h"
#include <algorithm>
#include <sstream>
#include <iostream>


AliasCommand::AliasCommand(vector<Command*> comms)
{
	_ids = { "alias" };
	_comms = comms;
}


AliasCommand::~AliasCommand()
{
}



void AliasCommand::AddID(string nuid) {
	_ids.push_back(nuid);
}


string AliasCommand::Syntax() {
	string mySyntax = "[ ";

	for (auto s : _ids) {
		mySyntax += s + " ";
	}
	mySyntax += "] [id of command you want to add id to]  _,";

	return mySyntax;
}

void AliasCommand ::Execute(Player *p, string text) {
	vector<string> command;
	bool change = false;

	std::transform(text.begin(), text.end(), text.begin(), ::tolower);

	istringstream iss(text);
	string nu;
	while (getline(iss, nu, ' ')) {

		command.push_back(nu);
	}

	if (command.size() == 3 && AreYou(command[0])) {

		if (AreYou(command[1])) {
			AddID(command[2]);
			change = true;
			cout << "Succcessfully added ID [" << command[2] << "] to command" << endl;
		}
		for (auto comm : _comms) {
			if (comm->AreYou(command[1])) {
				comm->AddID(command[2]);
				cout << "Succcessfully added ID [" << command[2] << "] to command" << endl;
				change = true;
			}
		}

		if (!change) {
			cout << "Could not find current command with ID [" << command[1] << endl;
		}

	}
	else {
		cout << "Incorrect format for Alias command" << endl;
	}



}

bool AliasCommand::AreYou(string objectID) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (objectID == _ids[i]) areYou = true;
	}
	return areYou;
}

void AliasCommand::AddCommand(Command* comm) {
	_comms.push_back(comm);
}