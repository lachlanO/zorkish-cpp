#pragma once
#include "OpenMessage.h"

class Blackboard
{
private:
	vector<OpenMessage> _OpenMessages;

	static bool instanceFlag;
	static Blackboard *single;
	Blackboard()
	{

	}
public:
	static Blackboard* getInstance();

	~Blackboard()
	{
		instanceFlag = false;
	}

	vector<OpenMessage>* GetOpenMessage();

	void SendOpenMessage(string command);

};

