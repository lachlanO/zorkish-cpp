#pragma once
#include <vector>
#include <string>
#include "Command.h"
#include "Player.h";

using namespace std;

class GoCommand: public Command
{
private:
	vector<string> _ids;
	Path getPath(Player*, string);

public:
	GoCommand();
	~GoCommand();
	void Execute(Player*, string);
	bool AreYou(string);
	string Syntax();
	void AddID(string);
	void AddCommand(Command*);

};

