#pragma once
#include "Player.h"
#include <string>

class Command
{
public:
	Command();
	~Command();
	virtual void Execute(Player*, string) = 0;
	virtual bool AreYou(string) = 0;
	virtual string Syntax() = 0;
	virtual void AddID(string) = 0;
	virtual void AddCommand(Command*) = 0;
};

