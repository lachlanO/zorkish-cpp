#pragma once
#include "IHeal.h"
#include <string>
#include <vector>
#include <stdlib.h>
#include "GameObject.h"
using namespace std;

class IHealComposite
{
private:
	vector<IHeal*> children;
	static bool instanceFlag;
	static IHealComposite *single;

public:
	void add(IHeal* ele);
	static IHealComposite* getInstance();
	void remove(IHeal* ele);
	~IHealComposite();
	bool Contains(GameObject* ele);

};