#include "SingletonHealth.h"



bool SingletonHealth::instanceFlag = false;
vector<string> SingletonHealth::_ids = { "singleHealth", "healingvec" };
SingletonHealth* SingletonHealth::single = NULL;
SingletonHealth* SingletonHealth::getInstance()
{
	if (!instanceFlag)
	{
		single = new SingletonHealth();
		instanceFlag = true;
		return single;
	}
	else
	{
		return single;
	}
}

bool SingletonHealth::AreYou(string id) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (id == _ids[i]) areYou = true;
	}
	return areYou;
}

vector<IHeal*> SingletonHealth::myList() {
	return _heals;
}


void SingletonHealth::add(IHeal* ele) {
	_heals.push_back(ele);
}


bool SingletonHealth::HasHeal(string id) {
	for (int i = 0; i < _heals.size(); i++)
	{
		if (_heals[i]->AreYou(id)) {
			return true;
		}
	}
	return false;
}

void SingletonHealth::Heal(string id) {
	bool itemHealed = false;
	if (HasHeal(id)) {
		for (auto s : _heals) {
			if (s->AreYou(id)) {
				s->Heal(id);
				itemHealed = true;
			}
		}
	}
	if (itemHealed) {
		remove(id);
	}
}

void SingletonHealth::remove(string id) {
	int removeint = 10000;
	for (int i = 0; i < _heals.size(); i++) {
		if (_heals[i]->AreYou(id)) {
			removeint = i;
		}
	}
	if (removeint != 10000) {
		auto it = next(_heals.begin(), removeint);
		_heals.erase(it);
	}
}
