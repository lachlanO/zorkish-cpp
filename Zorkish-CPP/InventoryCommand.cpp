#include "InventoryCommand.h"
#include <algorithm>
#include <sstream>
#include <iostream>

using namespace std;



InventoryCommand::InventoryCommand()
{
	_ids = { "inventory" };
}


InventoryCommand::~InventoryCommand()
{
}

void InventoryCommand::Execute(Player *p, string text) {
	vector<string> command;


	std::transform(text.begin(), text.end(), text.begin(), ::tolower);

	istringstream iss(text);
	string nu;
	while (getline(iss, nu, ' ')) {

		command.push_back(nu);
	}

	if (command.size() == 1) {
		if (AreYou(command[0])) {
			p->MyInventory()->ItemList();
		}
		else {
			cout << "Command does not use correct ALIAS of INVENTORY command";
		}
	}

	else {
		cout << "Invalid Inventory command" << endl;
	}
}

bool InventoryCommand::AreYou(string objectID) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (objectID == _ids[i]) areYou = true;
	}
	return areYou;
}

void InventoryCommand::AddID(string nuid) {
	_ids.push_back(nuid);
}


string InventoryCommand::Syntax() {
	string mySyntax = "[ ";

	for (auto s : _ids) {
		mySyntax += s + " ";
	}
	mySyntax += "]";

	return mySyntax;
}

void InventoryCommand::AddCommand(Command*) {

}
