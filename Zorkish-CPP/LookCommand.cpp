#include "LookCommand.h"
#include <sstream>
#include <algorithm>
#include <iostream>

using namespace std;

LookCommand::LookCommand()
{
	_ids = { "look" };
}


LookCommand::~LookCommand()
{
}

void LookCommand::Execute(Player* p, string text) {
	vector<string> command;
	

	string itemID = "";

	IHaveInventory* container;
	string containerID;

	std::transform(text.begin(), text.end(), text.begin(), ::tolower);

	istringstream iss(text);
	string nu;
	while (getline(iss, nu, ' ')) {

		command.push_back(nu);
	}

	if (AreYou(command[0])) {
		if (command[1] == "at") {
			switch (command.size()) {
			case 3:
				itemID = command[2];
				if (p->MyInventory()->HasItem(itemID)) {
					cout << "You Look at" << p->MyInventory()->Fetch(itemID)->GetName() << endl;
				}
				else {
					cout << "The item [" << itemID << "] could not be found" << endl;
				}
				break;

			case 5:
				cout << "This Will be implemented in the next section" << endl;
				break;

			default:
				cout << "Look command must be either 3 or 5 words in length" << endl;
				break;
			}
			
		}
		else if (command[1] == "in") {
			if (command.size() == 3) {
				containerID = command[2];
				container = fetchContainer(p, containerID);
				if (container != nullptr) {
					if (container->AreYouOpen()) {
						container->MyInventory()->ItemList();
					}
					else {
						cout << "[" << container->GetName() << "] is currently closed ;(" << endl;
					}
				}
			}
			else {
				cout << "Look in command requires 3 words ya dropkick" << endl;
			}
		}
		else {
			cout << "What do you want to look at or in?" << endl;
		}
	}
	else {
		cout << "Error in look input" << endl;
	}
	
}

bool LookCommand::AreYou(string objectID) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (objectID == _ids[i]) areYou = true;
	}
	return areYou;
}

void LookCommand::AddID(string nuid) {
	_ids.push_back(nuid);
}


string LookCommand::Syntax() {
	string mySyntax = "[ ";

	for (auto it = _ids.begin(); it != _ids.end(); ++it)
	{
		if (it == _ids.end())
		{
			mySyntax += *it;
		}
		else {
			mySyntax += *it + ", ";
		}
	}
	mySyntax += "] [at, in] _,";

	return mySyntax;
}


void LookCommand::AddCommand(Command*) {

}


IHaveInventory* LookCommand::fetchContainer(Player *p, string containerID) {
	if (p->AreYou(containerID)) {
		return p;
	}
	else if (p->MyLocation()->AreYou(containerID)) {
		return p->MyLocation();
	}
	else if (p->MyInventory()->HasItem(containerID)) {
		return p->Locate(containerID);
	}
	else {
		return nullptr;
	}
}