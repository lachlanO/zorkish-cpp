#include "OpenCommand.h"
#include <sstream>
#include <iostream>
#include <algorithm>



OpenCommand::OpenCommand()
{
	_ids = { "open" };
	_sender = OpenSender::getInstance();
	_blackboard = Blackboard::getInstance();
}


OpenCommand::~OpenCommand()
{
}

void OpenCommand::AddID(string nuid) {
	_ids.push_back(nuid);
}

string OpenCommand::Syntax() {
	string mySyntax = "[ ";

	for (auto it = _ids.begin(); it != _ids.end(); ++it)
	{
		if (it == _ids.end())
		{
			mySyntax += *it;
		}
		else {
			mySyntax += *it + ", ";
		}
	}
	mySyntax += "] _, [with] _,";

	return mySyntax;
}

bool OpenCommand::AreYou(string objectID) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (objectID == _ids[i]) areYou = true;
	}
	return areYou;
}


void OpenCommand::AddCommand(Command*) {

}


void OpenCommand::Execute(Player *p, string text) {
	vector<string> command;

	IHaveInventory *container;
	string containerID;
	string itemID = "";

	std::transform(text.begin(), text.end(), text.begin(), ::tolower);

	istringstream iss(text);
	string nu;
	while (getline(iss, nu, ' ')) {

		command.push_back(nu);
	}

	if (AreYou(command[0])) {
		switch (command.size()) {
		case 2:
			containerID = command[1];
			
			if (p->MyInventory()->HasItem(command[1])) {
				//_sender->sendMessage(command[1]);
				_blackboard->SendOpenMessage(command[1]);
			}
				
			break;

		case 4:
			itemID = command[3];
			containerID = command[1];
			container = fetchContainer(p, containerID);
			cout << "Keys and chests will be implemented in a later patch for the moment just stick to opening things with your hands (two word version of this command)" << endl;
			break;

		default:
			cout << "Invalid Open command [should either be 2 or 4 words in length]" << endl;
			break;
		}
	}
	else {
		cout << "Invalid Open command formatting" << endl;
	}

}



IHaveInventory* OpenCommand::fetchContainer(Player *p, string containerID) {
	if (p->AreYou(containerID)) {
		return p;
	}
	else if (p->MyLocation()->AreYou(containerID)) {
		return p->MyLocation();
	}
	else if (p->MyInventory()->HasItem(containerID)) {
		return p->Locate(containerID);
	}
	else {
		return nullptr;
	}
}