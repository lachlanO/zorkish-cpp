#pragma once

#include "state.h"
#include <iostream>
#include <ctime>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <array>

class help: public state
{
private:
	void validate();

public:
	help(statehandler *controler);
	~help();
	void Update();
	void Render();
};

