#include "HelpCommand.h"
#include <iostream>
#include <sstream>
#include <algorithm>

using namespace std;


HelpCommand::HelpCommand(vector<Command*> comms)
{
	_myCommands = comms;
	_ids = { "help" };
}


HelpCommand::~HelpCommand()
{
}

void HelpCommand::Execute(Player *p, string text) {
	vector<string> command;

	std::transform(text.begin(), text.end(), text.begin(), ::tolower);

	istringstream iss(text);
	string nu;
	while (getline(iss, nu, ' ')) {

		command.push_back(nu);
	}

	if (command.size() == 1 && AreYou(command[0])) {
		cout << endl << "Valid Commands" << endl;
		cout << this->Syntax() << endl;

		for (auto comms : _myCommands) {
			cout << comms->Syntax() << endl;
		}
	}
	else {
		cout << "Invalid help command" << endl;
	}
	
}


bool HelpCommand::AreYou(string objectID) {
	bool areYou = false;
	for (int i = 0; i < _ids.size(); i++) {
		if (objectID == _ids[i]) areYou = true;
	}
	return areYou;
}

void HelpCommand::AddID(string nuid) {
	_ids.push_back(nuid);
}


string HelpCommand::Syntax() {
	string mySyntax = "[ ";

	for (auto s : _ids) {
		mySyntax += s + " ";
	}
	mySyntax += "]";
	return mySyntax;
}


void HelpCommand::AddCommand(Command* comm) {
	_myCommands.push_back(comm);
}
