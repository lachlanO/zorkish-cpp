#pragma once

#include "state.h"
#include <iostream>
#include <ctime>
#include "time.h"
#include <sstream>
#include <stdlib.h>
#include <string>
#include <array>
#include "Location.h"

class adventure: public state
{
private:
	void validate();
	int command;
	vector<Location> _locations;
	void readFileAdventure1(string, string, string);
	

public:
	adventure(statehandler *controler);
	~adventure();
	void Update();
	void Render();
	
};

